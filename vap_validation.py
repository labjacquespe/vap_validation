#!/usr/bin/env python2

import sys
import fileinput
import subprocess
import tempfile
import os
import argparse

parser = argparse.ArgumentParser(description="VAP & GFV")
parser.add_argument("--vap", help="Path to the vap_core executable (default: %(default)s).", default="./vap_core")
parser.add_argument("--gfv", help="Path to the gfv executable (default: %(default)s).", default="./gfv")
parser.add_argument('file', nargs='?', type=argparse.FileType('r'), help="Parameters to read from. default: %(default)s.", default=sys.stdin)
args = parser.parse_args()

# Make sure an absolute path is used
args.gfv = os.path.realpath(args.gfv)
args.vap = os.path.realpath(args.vap)

dataset_paths=[]
refgroup_paths=[]
annotations_path=""
selection_path=""
exclusion_path=""

# Parse stdin or the file and extract file names
for line in args.file.readlines():
    if line:
        pass
    if line.startswith("~~@dataset_path="):
        if ":=:" not in line: dataset_paths.append(line[16:].strip())
        else: dataset_paths.append(line[line.find(":=:")+3:].strip())
    elif line.startswith("~~@refgroup_path="):
        if ":=:" not in line: refgroup_paths.append(line[17:].strip())
        else: refgroup_paths.append(line[line.find(":=:")+3:].strip())
    elif line.startswith("~~@annotations_path="):
        annotations_path = line[20:].strip()
    elif line.startswith("~~@selection_path="):
        selection_path = line[18:].strip()
    elif line.startswith("~~@exclusion_path="):
        exclusion_path = line[18:].strip()

# Create the file-formats file
list_formats=""
for d in dataset_paths: list_formats += (d + "\t" + "unknown\n")
for r in refgroup_paths: list_formats += (r + "\t" + "reference_annotations\n")
list_formats += (annotations_path + "\t" + "unknown\n")
if len(selection_path) > 0:
    list_formats += (selection_path + "\t" + "reference_filters\n")
if len(exclusion_path) > 0:
    list_formats += (exclusion_path + "\t" + "reference_filters\n")

# Create the file, it will be deleted at the end of this script
tf = tempfile.NamedTemporaryFile()
tf.write(list_formats)
tf.seek(0, os.SEEK_END)

# Call GFV on the file
retval = subprocess.call([args.gfv, "--list-formats", tf.name, "-l"])

# If everything was valid, run vap with stdin or filename
if not retval:
    if not sys.stdin.isatty():
        sys.stdin.seek(0) # IMPORTANT: reset stdin to read it again
        subprocess.call([args.vap])
    else:
        args.file.seek(0) # IMPORTANT: reset file to read it again
        subprocess.call([args.vap, args.file.name])
else:
    print "error: some files are invalid"
