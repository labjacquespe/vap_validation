VAP validation tool : VAP Utils
=====================================
Copyright (C) 2017 Charles Coulombe
Université de Sherbrooke. All rights reserved.

A validation script to validate [VAP](https://bitbucket.org/labjacquespe/vap) input files with the [Genomic Format Validator (GFV)](https://bitbucket.org/labjacquespe/gfv).

Usage
-----

```
usage: vap_validation.py [-h] [--vap VAP] [--gfv GFV] [file]

VAP & GFV

positional arguments:
  file        Parameters to read from. default: stdin

optional arguments:
  -h, --help  show this help message and exit
  --vap VAP   Path to the vap_core executable (default: ./vap_core).
  --gfv GFV   Path to the gfv executable (default: ./gfv).

```

Run
---

```
python vap_validation.py vap_parameters.txt;
python vap_validation.py < vap_parameters.txt;
python vap_validation.py --vap ~/vap_core/vap_core --gfv ~/gfv/gfv < vap_parameters.txt;
```

Bug Reports and Feedback
------------------------
You can report a bug [here](https://bitbucket.org/labjacquespe/vap_validation/issues).
You can also directly contact us via email by writing to vap_support at usherbrooke dot ca.

Any and all feedback is welcome.

Licensing
---------
VAP is released under the [GPL license](http://www.gnu.org/licenses/gpl-3.0.txt). For further details see the COPYING file
in this directory.
